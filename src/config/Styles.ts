enum Color {
    Basic = "#fff",
    Default = "000",
    Color1= "#F1ADBE",
    Color1Op1 = "rgba(241, 173, 190, 0.30)",
    ColorDefaultOp2 = "rgba(0, 0, 0, 0.80)",
    Color2 = "#CCCACC",
    Color3 = "#FFC200",
    Color4 = "#000099",
}

enum FontFamily {
    Default = "Roboto",
}

enum FontWeight {
    Ultra = "100",
    Thin = "300",
}

enum FontSize {
    Font1 = "30px",
    Font2 = "18px",
    Font3 = "14px",
    Font4 = "10px",
    Font5 = "24px",
    Font6 = "50px",
}

enum LetterSpacing {
    Normall = "0.07em",
}

enum Shadow {
    BoxShadow = "0 2px 4px 0 rgba(0,0,0,0.5)",
    ButtonShadow = "0 2px 4px 0 rgba(0,0,0,0.3)",
}

enum BorderRadius {
    Inputs = "4px",
}

export {Color, FontFamily, FontWeight, FontSize, LetterSpacing, Shadow, BorderRadius};
