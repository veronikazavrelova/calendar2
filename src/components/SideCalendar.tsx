import * as React from "react";
import styled from "styled-components";
import {Color, FontSize, FontWeight, Shadow} from "../config/Styles";
import {convertDayIndex, dateDiffInDays, getFirstDay} from "../functions/dateFunctions";
import {Modes} from "../functions/Modes";
import {months} from "../functions/translate";

const StyledSideCalendar = styled.div`
    display: inline-block;
    margin-bottom: 20px;
    border: 1px solid ${Color.Color1};
    box-shadow: ${Shadow.BoxShadow};
    background-color: ${Color.Basic};
    @media only screen and (max-width: 1379px) {
        width: 700px;
        margin: auto;
        margin-bottom: 20px;
        display: block;
    }
    @media only screen and (max-width: 720px) {
        width: 95%;
        margin: auto;
        margin-bottom: 20px;
        display: block;
    }
    @media only screen and (max-device-width: 720px) {
        width: 95%;
        margin: auto;
        margin-bottom: 20px;
        display: block;
    }
`;

const StyledWrap = styled.div`
    padding-bottom: 5px;
    @media (max-width: 1380px) {
        padding: 5px;
    }
`;

const StyledMonthYear = styled.h2`
    margin: 0;
    padding: 5px;
    color: ${Color.Color1};
    font-size: ${FontSize.Font2};
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font1};
    }
`;

interface IStyledCellProps {
    isActualMonth: boolean;
    isActualDay: boolean;
    isDrawWeek: boolean;
    isDrawDay: boolean;
    isWeekend: boolean;
}

const StyledCell = styled.div`
    width: ${100 / 7}%;
    height: 25px;
    display: inline-block;
    text-align: center;
    line-height: 25px;
    font-size: ${FontSize.Font3};
    background-color: ${(props: IStyledCellProps) =>
    props.isDrawWeek ?
        Color.Color1Op1 :
        (props.isDrawDay ? Color.Color1Op1 : Color.Basic)
    };
    color: ${(props: IStyledCellProps) => props.isActualDay ?
        Color.Basic :
        (props.isActualMonth ? Color.Default : Color.Color2)
    };
    @media (max-width: 1380px) {
        width: initial;
        padding: 0 5px;
        color: ${(props: IStyledCellProps) =>
            !props.isWeekend ? Color.Default :
            (props.isActualDay ? Color.Basic : Color.Color1)
        };
    }
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font5};
        padding: 5px 10px;
        height: 40px;
        line-height: 40px;
    }
`;

const StyledActualDay = styled.div`
    width: 25px;
    height: 25px;
    margin: auto;
    line-height: 25px;
    border-radius: 50%;
    background-color: ${Color.Color1};
    @media only screen and (max-device-width: 720px) {
        width: 40px;
        height: 40px;
        line-height: 40px;
    }
`;

interface ISideCalendarProps {
    actualDate: Date;
    actualMode: Modes;
    year: number;
    month: number;
    isDrawWeek?: boolean;
    isDrawDay?: boolean;
    isWeekend?: boolean;
}

class SideCalendar extends React.Component<ISideCalendarProps, {}> {
    public static defaultProps: Partial<ISideCalendarProps> = {
        isDrawDay: false,
        isDrawWeek: false,
        isWeekend: false,
    };
    public render() {
        const {actualDate, actualMode, year, month} = this.props;
        const cells = [];
        const firstDay = getFirstDay(month, year);
        let actualDrawDate = new Date();
        for (let i = 0; i < 42; i++) {
            // Podmienka pre zistenie aktualneho dna
            const isActualDay =
                firstDay.getFullYear() === new Date().getFullYear()
                && firstDay.getMonth() === new Date().getMonth()
                && firstDay.getDate() === new Date().getDate();
            // Podmienka pre zistenie vykreslovaneho dna
            actualDrawDate = new Date(actualDate.getTime());
            const isDrawDay =
                actualMode === Modes.DAY
                && firstDay.getDate() === actualDrawDate.getDate()
                && firstDay.getMonth() === actualDrawDate.getMonth()
                && firstDay.getFullYear() === actualDrawDate.getFullYear();
            // Podmienka pre zistenie vykreslovaneho tyzdna
            actualDrawDate = new Date(actualDate.getTime());
            const dayNumber = convertDayIndex(actualDrawDate.getDay());
            actualDrawDate.setDate(actualDrawDate.getDate() - dayNumber);
            const isDrawWeek =
                actualMode === Modes.WEEK
                && dateDiffInDays(actualDrawDate, firstDay) >= 0 && dateDiffInDays(actualDrawDate, firstDay) < 7;
            // podmienka pre zistenie soboty a nedele
            const isWeekend =
                firstDay.getDay() === 0
            || firstDay.getDay() === 6
            ;
            // podmienka pre vykreslovanie oznacenia aktulaneho dna
            let actualdayNumber;
            if (isActualDay) {
                actualdayNumber = (
                    <StyledActualDay>
                        {firstDay.getDate()}
                    </StyledActualDay>
                );
            } else {
                actualdayNumber = firstDay.getDate();
            }
            // VYKKRESLOVANIE JEDNOTLIVYCH BUNIEK MESIACA
            cells.push(
                <StyledCell
                    key={i}
                    isActualMonth={month === firstDay.getMonth()}
                    isActualDay={isActualDay}
                    isDrawWeek={isDrawWeek}
                    isDrawDay={isDrawDay}
                    isWeekend={isWeekend}
                >
                    {actualdayNumber}
                </StyledCell>,
            );
            firstDay.setDate(firstDay.getDate() + 1);
        }
        return (
            <StyledSideCalendar>
                <StyledWrap>
                <StyledMonthYear>{months[month]} {year}</StyledMonthYear>
                    {cells}
                </StyledWrap>
            </StyledSideCalendar>
        );
    }
}

export default SideCalendar;
