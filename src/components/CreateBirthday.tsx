import * as React from "react";
import {FormattedMessage} from "react-intl";
import styled from "styled-components";
import {Color, FontSize, Shadow} from "../config/Styles";
import {Languages} from "../functions/Languages";
import FormBirthday from "./FormBirthday";

const StyledWrap = styled.span`
    position: absolute;
    @media only screen and (max-width: 1379px) {
        position: relative;
    }
`;
const StyledModalWindow = styled.div`
    position:fixed;
    display: inline-block;
    @media only screen and (max-width: 1379px) {
        width: 100%;
        min-height: 100%;
        height: auto;
        top: 60px;
        left: 0;
        background-color: ${Color.ColorDefaultOp2};
    }
`;

const StyledCreateBirthday = styled.div`
    width: 350px;
    height: 610px;
    background-color: ${Color.Basic};
    box-shadow: ${Shadow.BoxShadow};
    border: 1px solid ${Color.Color1};
    @media only screen and (max-width: 1379px) {
        position: relative;
        display: block;
        margin: 100px auto;
    }
    @media only screen and (max-device-width: 720px) {
        width: 90%;
        min-height: 1500px;
        height: auto;
        margin: 5% auto;
        margin-top: 150px;
        display: block;
        position: relative;
    }
`;

const StyledBirthdayHead = styled.div`
    background-color: ${Color.Color1};
    border: 1px solid ${Color.Color1};
    color: ${Color.Basic};
    font-size: ${FontSize.Font2};
    text-align: center;
    @media only screen and (max-device-width: 720px) {
        padding: 20px;
        font-size: ${FontSize.Font6};
    }
`;

const StyledClose = styled.div`
    width: 10px;
    height: 12px;
    position: absolute;
    right: 10px;
    top: 8px;
    background-image: url("src/images/x.svg");
    background-repeat: no-repeat;
    background-size: contain;
    &:hover {
        width: 12px;
        height: 14px;
        top: 7px;
    }
    @media only screen and (max-device-width: 720px) {
        width: 30px;
        height: 30px;
        top: 45px;
        right: 20px;
        &:hover {
            width: 30px;
            height: 30px;
            top: 45px;
            right: 20px;
        }
    }
`;

const StyledOpenAddFriends = styled.div`
    margin: 0 10px;
    padding: 20px 10px;
    text-align: right;
    font-size: ${FontSize.Font2};
    border-bottom: 1px solid ${Color.Color1};
    &:hover {
        color: ${Color.Color2};
    }
    cursor: pointer;
    @media only screen and (max-device-width: 720px) {
        width: 90%;
        margin: 0 5%;
        padding: 40px 0;
        font-size: ${FontSize.Font1};
    }
`;

const StyledOpenMyFriends = StyledOpenAddFriends.extend`
   width: 85%;
   position: absolute;
   margin: 0 5%;
   bottom: 0;
   right: 0;
   border-bottom: none;
   border-top: 1px solid ${Color.Color1};
   @media only screen and (max-device-width: 720px) {
        width: 90%;
        margin: 0 5%;
        padding: 40px 0;
        right: initial;
    }
`;

const StyledForm = styled.div`
    margin: 0 10px;
`;

const StyledMyFriends = styled.div`
    height: 470px;
    padding: 10px;
    @media only screen and (max-device-width: 720px) {
        height: 90%;
        padding: 40px 5%;
    }
`;

const StyledMyFriendsList = styled.ul`
    height: 100%;
    margin: 0;
    overflow: auto;
    font-size: ${FontSize.Font3};
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font1};
    }
`;

const StyledMyFriendsItemWrap = styled.li`
    padding-bottom: 10px;
`;

const StyledMyFriendsItem = styled.div`
    display: inline-block;
    padding-right: 20px;
    @media only screen and (max-device-width: 720px) {
        padding-right: 50px;
    }
`;

const StyledBasket = styled.div`
    width: 16px;
    height: 18px;
    display: inline-block;
    position: relative;
    top: 3px;
    background-image: url("src/images/basket.svg");
    background-size: contain;
    background-repeat: no-repeat;
    visibility: hidden;
    ${StyledMyFriendsItemWrap}:hover & {
        visibility: visible;
    }
    &:hover{
        background-image: url("src/images/basketActive.svg");
    }
    @media only screen and (max-device-width: 720px) {
        width: 30px;
        height: 30px;
    }
`;

interface ICreateBirthdayProps {
    toggleIsOpenBirthday: () => void;
    actualLanguage: Languages;
}

interface ICreateBirthdayState {
    isOpenFriends: boolean;
}

class CreateBirthday extends React.Component <ICreateBirthdayProps, ICreateBirthdayState> {
    constructor(props) {
        super(props);
        this.state = {
            isOpenFriends: true,
        };
        this.toggleIsOpenFriends = this.toggleIsOpenFriends.bind(this);
    }
    public render() {
        const {toggleIsOpenBirthday} = this.props;
        const {isOpenFriends} = this.state;
        // testovacie pole - zoznam mojich priatelov
        const myFriends = [];
        for (let i = 0; i < 50; i++) {
            myFriends.push(
                <StyledMyFriendsItemWrap key={i}>
                    <StyledMyFriendsItem>{"Veronika Zavrelova 21"}</StyledMyFriendsItem>
                    <StyledBasket/>
                </StyledMyFriendsItemWrap>,
            );
        }
        myFriends.push(
            <StyledMyFriendsItem key={4}>{"Marek Cepcek 21"}</StyledMyFriendsItem>,
        )
        return(
            <StyledWrap>
                <StyledModalWindow/>
                <StyledCreateBirthday>
                    {/*hlavicka*/}
                    <StyledBirthdayHead>
                        <FormattedMessage id={"friends"}/>
                        <StyledClose
                            onClick={() => {
                                toggleIsOpenBirthday();
                            }}
                        />
                    </StyledBirthdayHead>
                    {/*pridavanie priatelov*/}
                    {isOpenFriends &&
                        <StyledOpenAddFriends
                            onClick={() => {
                                this.toggleIsOpenFriends();
                            }}
                        >
                            {"+ "}
                            <FormattedMessage id={"addFriends"} defaultMessage={"add new friends"}/>
                        </StyledOpenAddFriends>
                    }
                    {/*formular pre pridavanie priatelov*/}
                    {!isOpenFriends &&
                        <StyledForm>
                            <FormBirthday
                                actualLanguage={this.props.actualLanguage}
                            />
                        </StyledForm>
                    }
                    {/*{moji priatelia}*/}
                    {!isOpenFriends &&
                        <StyledOpenMyFriends
                            onClick={() => {
                                this.toggleIsOpenFriends();
                            }}
                        >
                            <FormattedMessage id={"myFriends"} defaultMessage={"my friends"}/>
                        </StyledOpenMyFriends>
                    }
                    {/*zoznam mojich priatelov*/}
                    {isOpenFriends &&
                    <StyledMyFriends>
                        <StyledMyFriendsList>
                            {myFriends}
                        </StyledMyFriendsList>
                    </StyledMyFriends>
                    }
                </StyledCreateBirthday>
            </StyledWrap>
        );
    }
    public toggleIsOpenFriends() {
        this.setState((prev) => ({
            isOpenFriends: !prev.isOpenFriends,
        }));
    }
}

export default CreateBirthday;
