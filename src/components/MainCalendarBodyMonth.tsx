import * as React from "react";
import styled from "styled-components";
import {Color, FontSize, FontWeight} from "../config/Styles";
import {convertDayIndex, drawIssues, getFirstDay} from "../functions/dateFunctions";
import {initHolidays} from "../functions/Holidays";
import {Languages} from "../functions/Languages";
import {Modes} from "../functions/Modes";
import {names} from "../functions/Names";
import {weakDays} from "../functions/translate";
import DayCellHover from "./DayCellHover";
import {Layout} from "../functions/Layout";

interface IStyledDayCellProps {
    isActualMonth: boolean;
}

const StyledCellWrap = styled.div`
    @media only screen and (max-device-width: 720px) {
            height: 700px;
        }
`;

const StyledDayCell = styled.div`
    width: ${100 / 7}%;
    height: 85px;
    position: relative;
    display: inline-block;
    border-left: 1px solid ${Color.Color1};
    border-bottom: 1px solid ${Color.Color1};
    :nth-child(7n + 0){
        border-right: 1px solid ${Color.Color1};
    };
    background-color: ${Color.Basic};
    box-sizing: border-box;
    text-align: right;
    color: ${(props: IStyledDayCellProps) => props.isActualMonth ? Color.Default : Color.Color2
    };
    cursor: pointer;
    span {
        visibility: hidden;
    };
    &:hover {
        span {
        visibility: visible;
        };
    };
    @media only screen and (max-device-width: 720px) {
        height: ${100 / 6}%;
        float: left;
    }
`;
const StyledBirthdayDot = styled.div`
    width: 10px;
    height: 10px;
    border-radius: 50%;
    background-color: ${Color.Color3};
    display: inline-block;
    position: absolute;
    top: 5px;
    left: 5px;
    @media only screen and (max-width: 550px) {
        width: 5px;
        height: 5px;
    }
`;

const StyledNoteDot = StyledBirthdayDot.extend`
    background-color: ${Color.Color4};
    left: 20px;
    @media only screen and (max-width: 550px) {
        left: 15px;
    }
`;

interface IStyledNumberProps {
    isActualDay: boolean;
}

const StyledNumber = styled.div`
    width: 28px;
    display: inline-block;
    padding: 5px;
    font-size: ${FontSize.Font2};
    color: ${(props: IStyledNumberProps) => props.isActualDay ? Color.Basic : Color.Default};
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font1};
        width: 50px;
        padding: 5px 15px;
    }
`;

const StyledActualDay = styled.div`
    display: block;
    line-height: 28px;
    text-align: center;
    background-color: ${Color.Color1};
    border-radius: 50%;
    div {
        padding-left: 1px;
    }
    @media only screen and (max-device-width: 720px) {
         line-height: 50px;
    }
`;
const StyledNames = styled.div`
    position: absolute;
    bottom: 0;
    right: 0;
    font-size: ${FontSize.Font4};
    font-weight: ${FontWeight.Thin};
    text-align: right;
    padding: 5px;
    @media only screen and (max-width: 550px) {
        white-space: nowrap;
        width: 6em;
        overflow: hidden;
    }
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font2};
        width: 6em;
    }
`;

const StyledHolidays = StyledNames.extend`
    top: 35px;
    white-space: nowrap;
    width: 8em;
    overflow: hidden;
    text-overflow: ellipsis;
    @media only screen and (max-width: 550px) {
        top: 30px;
    }
    @media only screen and (max-device-width: 720px) {
        top: 45px;
    }
`;

interface IMainCalendarBodyMonthProps {
    layout: Layout;
    year: number;
    month: number;
    actualLanguage: Languages;
    actualDate: Date;
    setActualMode: (newMode: Modes) => void;
    setActualDate: (newDate: Date) => void;
    isLogin: boolean;
    isOpenNote: boolean;
    isOpenBirthday: boolean;
}

class MainCalendarBodyMonth extends React.Component <IMainCalendarBodyMonthProps, {}> {
    public render() {
        const {layout, year, month, actualLanguage, actualDate, setActualMode, setActualDate, isLogin, isOpenNote, isOpenBirthday} = this.props;
        const firstDay = getFirstDay(month, year);
        const cells = [];
        for (let i = 0; i < 42; i++) {
            // konstanta, v ktorej je ulozeny aktualne vykresleny firstday;
            const actullDay = new Date(firstDay.getTime());
            // Podmienka pre zistenie aktualneho dna
            const isActualDay =
                firstDay.getFullYear() === new Date().getFullYear()
                && firstDay.getMonth() === new Date().getMonth()
                && firstDay.getDate() === new Date().getDate();
            // Podmienka pre vykreslovanie oznacenia aktulaneho dna
            let actualDayNumber;
            if (isActualDay) {
                actualDayNumber = (
                    <StyledActualDay>
                        <div>
                            {firstDay.getDate()}
                        </div>
                    </StyledActualDay>
                );
            } else {
                actualDayNumber = firstDay.getDate();
            }
            // VYKKRESLOVANIE JEDNOTLIVYCH BUNIEK MESIACA
            cells.push(
                <StyledDayCell
                    key={i}
                    isActualMonth={month === firstDay.getMonth()}
                    onClick={() => {
                        setActualMode(Modes.DAY);
                        setActualDate(actullDay);
                    }}
                >
                    {/*Vykreslenie puntika pre narodeniny, zobrazi sa len pri stave LOG IN*/}
                    {isLogin &&
                        (<StyledBirthdayDot/>)
                    }
                    {/*Vykreslenie puntika pre poznamku, zobrazi sa len pri stave LOG IN*/}
                    {isLogin &&
                    (<StyledNoteDot/>)
                    }
                    {/*Vykreslenie datumu*/}
                    <StyledNumber isActualDay={isActualDay}>
                        {actualDayNumber}
                    </StyledNumber>
                    {/*Vykreslenie sviatkov*/}
                    <StyledHolidays>
                        {drawIssues(initHolidays(actualDate), actualLanguage, firstDay)}
                    </StyledHolidays>
                    {/*Vykreslenie menin*/}
                    <StyledNames>
                        {drawIssues(names, actualLanguage, firstDay).split(", ")[0]}
                    </StyledNames>
                    {/*Vykreslenie hoveru bunky dna*/}
                    {!isOpenNote && !isOpenBirthday && layout === Layout.DESKTOP &&
                        <span>
                            <DayCellHover
                                dayNumber={firstDay.getDate()}
                                dayName={weakDays[convertDayIndex(firstDay.getDay())]}
                            />
                        </span>
                    }
                </StyledDayCell>,
                );
            firstDay.setDate(firstDay.getDate() + 1);
        }
        return (
            <StyledCellWrap>{cells}</StyledCellWrap>
        );
    }
}

export default MainCalendarBodyMonth;
