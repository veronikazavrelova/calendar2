import * as React from "react";
import {FormattedMessage, InjectedIntlProps, injectIntl} from "react-intl";
import styled from "styled-components";
import {Color, FontSize, Shadow} from "../config/Styles";

const StyledDayCellHover = styled.div`
    width: 270px;
    height: 540px;
    position: fixed;
    top: 140px;
    left: 1007px;
    background-color: ${Color.Basic};
    border: 1px solid ${Color.Color1};
    box-shadow: ${Shadow.BoxShadow};
    z-index: 1000;
`;

const StyledDayName = styled.div`
    height: 30px;
    background-color: ${Color.Color1};
    color: ${Color.Basic};
    font-size: ${FontSize.Font2};
    line-height: 30px;
    text-align: center;
`;

const StyledIssuesWrap = styled.div`
    max-height: 430px;
    margin: 10px;
    overflow: auto;
    text-align: left;
    font-size: ${FontSize.Font2};
`;

const StyledIssue = styled.div`
    padding-bottom: 10px;
`;

interface IDayNameProps {
    dayNumber: number;
    dayName: JSX.Element;
}

const DayCellHover = injectIntl<IDayNameProps>(class extends React.Component <IDayNameProps & InjectedIntlProps, {}> {
    public render() {
        return (
          <StyledDayCellHover>
              <StyledDayName>{this.props.dayNumber}{". "}{this.props.dayName}</StyledDayName>
              <StyledIssuesWrap>
                  <StyledIssue>
                      <FormattedMessage id={"cellHoverMessage"} defaultMessage={"Here you will see your reminder."}/>
                  </StyledIssue>
              </StyledIssuesWrap>
          </StyledDayCellHover>
        );
    }
});

export default DayCellHover;
