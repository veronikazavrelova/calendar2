import * as React from "react";
import styled from "styled-components";
import {Color, FontFamily, FontSize, FontWeight, LetterSpacing, Shadow} from "../config/Styles";
import {Simulate} from "react-dom/test-utils";
import keyDown = Simulate.keyDown;

enum ButtonWidth {
    Default,
    MinWidth,
}

enum ButtonShadow {
    Default,
    BoxShadow,
}

enum ButtonFontSize {
    Default,
    Submit,
}
enum ButtonPadding {
    Default,
    Submit,
}

interface IStyledButtonProps {
    shadow: ButtonShadow;
    width: ButtonWidth;
    isActive: boolean;
    fontSize: ButtonFontSize;
    buttonPadding: ButtonPadding;
}

const StyledButton = styled.button`
    min-width: ${(props: IStyledButtonProps) => {
        switch (props.width) {
            case ButtonWidth.Default:
                return "initial";
            case ButtonWidth.MinWidth:
                return "120px";
            }
    }};
    padding: 5px;
    display: inline-block;
    border: 1px solid ${Color.Color1};
    background-color: ${(props: IStyledButtonProps) => {
        return props.isActive ? Color.Basic : Color.Color1;
    }};
    color: ${(props: IStyledButtonProps) => {
        return props.isActive ? Color.Color1 : Color.Basic;
    }};
    font-size: ${FontSize.Font2};
    font-weight: ${FontWeight.Ultra};
    text-align: center;
    line-height: 18px;
    box-shadow: ${(props: IStyledButtonProps) => {
        switch (props.shadow) {
            case ButtonShadow.Default:
                return "none";
            case ButtonShadow.BoxShadow:
                return Shadow.BoxShadow;
        }
    }};
    &:hover {
    background-color: ${Color.Basic};
    color: ${Color.Color1};
    }
    cursor: pointer;
    @media (max-width: 720px) {
        font-size: ${FontSize.Font3};
    }
    @media only screen and (max-device-width: 720px) {
        font-size: ${(props: IStyledButtonProps) => {
            switch (props.fontSize) {
                case ButtonFontSize.Default:
                    return FontSize.Font5;
                case ButtonFontSize.Submit:
                    return FontSize.Font6;
            }
        }};
        padding: ${(props: IStyledButtonProps) => {
            switch (props.buttonPadding) {
                case ButtonPadding.Default:
                    return "10px";
                case ButtonPadding.Submit:
                    return "40px";
            }
        }};
    }
`;

interface IButtonProps {
    shadow?: ButtonShadow;
    width?: ButtonWidth;
    onClick?: () => void;
    isActive?: boolean;
    type?: "button" | "submit" | "reset";
    fontSize?: ButtonFontSize;
    buttonPadding?: ButtonPadding;
}

class Button extends React.Component<IButtonProps, {}> {
    public static defaultProps: Partial<IButtonProps> = {
        isActive: false,
        shadow: ButtonShadow.Default,
        type: "button",
        width: ButtonWidth.Default,
        fontSize: ButtonFontSize.Default,
        buttonPadding: ButtonPadding.Default,
    };
    public render() {
        return (
            <StyledButton
                width={this.props.width}
                shadow={this.props.shadow}
                onClick={this.props.onClick}
                isActive={this.props.isActive}
                type={this.props.type}
                fontSize={this.props.fontSize}
                buttonPadding={this.props.buttonPadding}
            >
                {this.props.children}
            </StyledButton>
        );
    }
}

export default Button;
export {ButtonWidth, ButtonShadow, ButtonFontSize, ButtonPadding};
