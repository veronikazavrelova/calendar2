import * as React from "react";
import styled from "styled-components";
import {Color, FontFamily, FontSize, FontWeight, LetterSpacing} from "../config/Styles";
import {weakDays} from "../functions/translate";
const StyledWrap = styled.div`
    line-height: initial;
`;

const StyledWeekDay = styled.div`
    width: ${100 / 7}%;
    height: 30px;
    display: inline-block;
    background-color: ${Color.Color1};
    color: ${Color.Basic};
    font-size: ${FontSize.Font2};
    line-height: 30px;
    text-align: center;
    @media (max-width: 720px) {
        font-weight: ${FontWeight.Thin};
        font-size: ${FontSize.Font3};
    }
`;

class Week extends React.Component <{}, {}> {
    public render() {
        const dayNames = [];
        for (let i = 0; i < 7; i++) {
            dayNames.push(
                <StyledWeekDay key={i}>{weakDays[i]}</StyledWeekDay>,
            );
        }
        return(
            <StyledWrap>{dayNames}</StyledWrap>
        );
    }
}

export default Week;
