import * as React from "react";
import {FormattedMessage} from "react-intl";
import styled from "styled-components";
import {Color, FontSize, Shadow} from "../config/Styles";
import FormNote from "./FormNote";

const StyledWrap = styled.span`
    position: absolute;
    @media only screen and (max-width: 1379px) {
        position: relative;
    }
`;

const StyledModalWindow = styled.div`
    position: fixed;
    display: inline-block;
    @media (max-width: 1379px) {
        width: 100%;
        min-height: 100%;
        height: auto;
        top: 60px;
        left: 0;
        background-color: ${Color.ColorDefaultOp2};
    }
`;

const StyledCreateNotification = styled.div`
    width: 350px;
    height: 610px;
    position: absolute;
    display: inline-block;
    background-color: ${Color.Basic};
    box-shadow: ${Shadow.BoxShadow};
    border: 1px solid ${Color.Color1};
    @media only screen and (max-width: 1379px) {
        position: relative;
        display: block;
        margin: 100px auto;
    }
    @media only screen and (max-device-width: 720px) {
        width: 90%;
        min-height: 1500px;
        height: auto;
        margin: 5% auto;
        margin-top: 150px;
        display: block;
        position: relative;
    }
`;

const StyledNotificationHead = styled.div`
    background-color: ${Color.Color1};
    border: 1px solid ${Color.Color1};
    color: ${Color.Basic};
    font-size: ${FontSize.Font2};
    text-align: center;
    @media only screen and (max-device-width: 720px) {
        padding: 20px;
        font-size: ${FontSize.Font6};
    }
`;

const StyledClose = styled.div`
    width: 10px;
    height: 12px;
    position: absolute;
    right: 10px;
    top: 8px;
    background-image: url("src/images/x.svg");
    background-repeat: no-repeat;
    background-size: contain;
    &:hover {
        width: 12px;
        height: 14px;
         top: 7px;
    }
    @media only screen and (max-device-width: 720px) {
        width: 30px;
        height: 30px;
        top: 45px;
        right: 20px;
        &:hover {
            width: 30px;
            height: 30px;
            top: 45px;
            right: 20px;
        }
    }
`;

interface ICreateNotificationProps {
    toggleIsOpenNote: () => void;
}

class CreateNotification extends React.Component <ICreateNotificationProps, {}> {
    public render() {
        const {toggleIsOpenNote} = this.props;
        return(
            <StyledWrap>
                <StyledModalWindow/>
                <StyledCreateNotification>
                    {/*hlavicka*/}
                    <StyledNotificationHead>
                        <FormattedMessage id={"NewNotofication"} defaultMessage={"new reminder"}/>
                        <StyledClose
                            onClick={() => {
                                toggleIsOpenNote();
                            }}
                        />
                    </StyledNotificationHead>
                    {/*formular*/}
                    <FormNote/>
                </StyledCreateNotification>
            </StyledWrap>
        );
    }
}

export default CreateNotification;
