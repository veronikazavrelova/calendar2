import * as React from "react";
import styled from "styled-components";
import {Languages} from "../functions/Languages";
import {languages} from "../functions/translate";
import Button, {ButtonWidth} from "./Button";
import Icon, {BackgroundSize, IconImage} from "./Icon";
import IconLogin from "./IconLogin";
import LanguagesList from "./LanguagesList";
import NavigationItem, {BoxShadow} from "./NavigationItems";
import {Layout} from "../functions/Layout";

const StyledNavigation = styled.ul`
    margin: 5px 0;
    float: right;
`;

interface INavigationProps {
    actualLanguage: Languages;
    setActualLanguage: (newLanguage: Languages) => void;
    isLogin: boolean;
    toggleIsLogin: () => void;
    isOpenBirthday: boolean;
    toggleIsOpenBirthday: () => void;
    isOpenNote: boolean;
    toggleIsOpenNote: () => void;
}

class Navigation extends React.Component<INavigationProps, {}> {
    public render() {
        const {actualLanguage, setActualLanguage, isLogin, toggleIsLogin, isOpenBirthday, toggleIsOpenBirthday, isOpenNote, toggleIsOpenNote} =  this.props;
        // Podmienka overenie prihlasenia
        let logNavigation;
        if (isLogin)  {
            logNavigation = (
                <span>
                    <NavigationItem>
                        <Icon
                            isActive={isOpenBirthday}
                            iconImage={IconImage.birthday}
                            onClick={() => {
                                toggleIsOpenBirthday();
                                isOpenNote && toggleIsOpenNote();
                            }}
                        />
                    </NavigationItem>
                    <NavigationItem>
                        <Icon
                            isActive={isOpenNote}
                            iconImage={IconImage.note}
                            onClick={() => {
                                toggleIsOpenNote();
                                isOpenBirthday && toggleIsOpenBirthday();
                            }}
                        />
                    </NavigationItem>
                </span>
            );
        } else {
            logNavigation = (
                <NavigationItem>
                    <Icon
                        isActive={isLogin}
                        iconImage={IconImage.login}
                        onClick={() => {
                            toggleIsLogin();
                        }}
                        backgroundSize={BackgroundSize.contain}
                    />
                </NavigationItem>
            );
        }
        return (
            <StyledNavigation>
                {logNavigation}
                <NavigationItem shadow={BoxShadow.BoxShadow}>
                    <Button width={ButtonWidth.MinWidth}>{languages[actualLanguage]}</Button>
                    <div className={"hover"}>
                        <LanguagesList setActualLanguage={setActualLanguage}/>
                    </div>
                </NavigationItem>
            </StyledNavigation>
        );
    }
}

export default Navigation;
