import * as React from "react";
import {FormattedMessage} from "react-intl";
import styled from "styled-components";
import {Color, FontSize} from "../config/Styles";
import {convertDayIndex} from "../functions/dateFunctions";
import {Modes} from "../functions/Modes";
import {months} from "../functions/translate";
import Button from "./Button";

const StyledMainContainerHead = styled.div`
    height: 70px;
    border: 1px solid ${Color.Color1};
`;

const StyledMonthNameYear = styled.h1`
    width: ${100 / 7 * 3}%;
    float: left;
    color: ${Color.Color1};
    font-size: ${FontSize.Font1};
    text-align: center;
    line-height: 70px;
    @media only screen and (max-width: 720px) {
        width: 150px;
        text-align: left;
        padding-left: 20px;
        font-size: ${FontSize.Font2};
    }
    @media only screen and (max-device-width: 720px) {
        width: 300px;
        text-align: left;
        padding-left: 5%;
    }
`;

const StyledButtonGroupWrap = styled.div`
    width: ${100 / 7 * 2}%;
    float: left;
    height: 100%;
    text-align: center;
    @media only screen and (max-width: 720px) {
        width: initial;
        padding-left: 5%;
    }
    @media only screen and (max-device-width: 720px){
        width: initial;
        padding-left: 5%;
    }
`;

const StyledButtonGroup = styled.span`
    margin-top: 21px;
    display: inline-block;
    span {
        margin-right: 1px;
        @media only screen and (max-device-width: 720px){
            margin-right: 3px;
        }
        &:last-child {
            margin-right: 0;
        }
    }
    @media only screen and (max-device-width: 720px){
        margin-top: 15px;
    }
`;

interface IMainCalendarHeadProps {
    year: number;
    actualDate: Date;
    actualMode: Modes;
    setActualDate: (newDate: Date) => void;
    setActualMode: (newMode: Modes) => void;
}

class MainCalendarHead extends React.Component <IMainCalendarHeadProps, {}> {
    public render() {
        const {year, actualDate, actualMode, setActualDate, setActualMode} = this.props;
        let actualDrawDate = new Date();
        const prevMode = actualMode;
        return (
            <StyledMainContainerHead>
                {/*Vykreslenie nazvu mesiaca a roka*/}
                <StyledMonthNameYear>{months[actualDate.getMonth()]} {year}</StyledMonthNameYear>
                {/*Vykreslenie prvej serie tlacidiel*/}
                <StyledButtonGroupWrap>
                    <StyledButtonGroup>
                        <span>
                            {/*Vykreslenie tlacidla DAY*/}
                            <Button
                                isActive={actualMode === Modes.DAY}
                                onClick={() => {
                                    setActualMode(Modes.DAY);
                                    setActualDate(new Date());
                                    }}
                            >
                                <FormattedMessage id={"day"}/>
                            </Button>
                        </span>
                        <span>
                            {/*Vykreslenie tlacidla WEEK*/}
                            <Button
                                isActive={actualMode === Modes.WEEK}
                                onClick={() => {
                                    setActualMode(Modes.WEEK);
                                    if (prevMode === Modes.DAY) {
                                        actualDrawDate = new Date(actualDate.getTime());
                                    }
                                    actualDrawDate.setDate(actualDrawDate.getDate() - this.dayNumber(actualDrawDate));
                                    setActualDate(actualDrawDate);
                                }}
                            >
                                <FormattedMessage id={"week"}/>
                            </Button>
                        </span>
                        <span>
                            {/*Vykreslenie tlacidla MONTH*/}
                            <Button
                                isActive={actualMode === Modes.MONTH}
                                onClick={() => {setActualMode(Modes.MONTH); }}
                            >
                                <FormattedMessage id={"month"}/>
                            </Button>
                        </span>
                    </StyledButtonGroup>
                </StyledButtonGroupWrap>
                {/*Vykreslenie druhej serie tlacidiel*/}
                <StyledButtonGroupWrap>
                    <StyledButtonGroup>
                        <span>
                            {/*Vykreslenie tlacidla <*/}
                            <Button
                                onClick={() => {
                                    this.onClickLeft(actualDrawDate, actualDate, actualMode);
                                }}
                            >
                                {"<"}
                            </Button>
                        </span>
                        <span>
                            {/*Vykreslenie tlacidla TODAY*/}
                            <Button
                                onClick={() => {
                                    actualDrawDate = new Date();
                                    if (actualMode === Modes.WEEK) {
                                        actualDrawDate.setDate(actualDrawDate.getDate() - this.dayNumber(actualDrawDate));
                                    }
                                    setActualDate(actualDrawDate);
                                }}
                            >
                                <FormattedMessage id={"today"} defaultMessage={"today"}/>
                            </Button>
                        </span>
                        <span>
                            {/*Vykreslenie tlacidla >*/}
                            <Button
                                onClick={() => {
                                    this.onClickRight(actualDrawDate, actualDate, actualMode);
                                }}
                            >
                                {">"}
                            </Button>
                        </span>
                    </StyledButtonGroup>
                </StyledButtonGroupWrap>
            </StyledMainContainerHead>
        );
    }
    private dayNumber(drawDate) {
        return convertDayIndex(drawDate.getDay());
    }
    private onClickLeft(actualDrawDate, actualDate, actualMode) {
        actualDrawDate = new Date(actualDate.getTime());
        switch (actualMode) {
            case Modes.MONTH:
                actualDrawDate.setDate(0);
            case Modes.WEEK:
                actualDrawDate.setDate(actualDrawDate.getDate() - 6);
            case Modes.DAY:
                actualDrawDate.setDate(actualDrawDate.getDate() - 1);
                break;
        }
        this.props.setActualDate(actualDrawDate);
    }
    private onClickRight(actualDrawDate, actualDate, actualMode) {
        actualDrawDate = new Date(actualDate.getTime());
        switch (actualMode) {
            case Modes.MONTH:
                actualDrawDate.setDate(32);
            case Modes.WEEK:
                actualDrawDate.setDate(actualDrawDate.getDate() + 6);
            case Modes.DAY:
                actualDrawDate.setDate(actualDrawDate.getDate() + 1);
                break;
        }
        this.props.setActualDate(actualDrawDate);
    }
}

export default MainCalendarHead;
