import * as React from "react";
import styled from "styled-components";
import {Color, FontFamily, FontSize, FontWeight} from "../config/Styles";
import {InputHTMLAttributes} from "react";

const StyledLabel = styled.label`
    display: block;
    padding-bottom: 10px;
    font-size: ${FontSize.Font3};
     @media only screen and (max-device-width: 720px) {
     padding-bottom: 30px;
        font-size: ${FontSize.Font6};
    }
`;

const StyledInput = styled.input`
    width: 90%;
    padding: 0 5%;
    border: none;
    border-bottom: 1px solid ${Color.Color2};
    color: ${Color.Default};
    font-size: ${FontSize.Font3};
    font-weight: ${FontWeight.Thin};
    &::placeholder {
        color: ${Color.Color2};
    }
     @media only screen and (max-device-width: 720px) {
     margin: auto;
        font-size: ${FontSize.Font6};
    }
`;

enum InputType {
    Text = "text",
    Date = "date",
    Time = "time",
}

class Input extends React.Component <InputHTMLAttributes<HTMLInputElement>, {}> {
    public render() {
        const {children, ...props} = this.props;
        return(
            <span>
                <StyledLabel>
                    {children}
                </StyledLabel>
                <StyledInput {...props}/>
            </span>
        );
    }
}

export default Input;
export {InputType};
