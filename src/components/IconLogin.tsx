import * as React from "react";
import styled from "styled-components";

const StyledIconLogin = styled.div`
    width: 28px;
    height: 28px;
    background-image: url("src/images/person.svg");
    &:hover {
        background-image: url("src/images/personActive.svg");
    }
`;

interface IIconLoginProps {
    onClick?: () => void;
}

class IconLogin extends React.Component<IIconLoginProps, {}> {
    public render() {
        return (
            <StyledIconLogin onClick={this.props.onClick}/>
        );
    }
}

export default IconLogin;
