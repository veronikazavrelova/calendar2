import * as React from "react";
import styled from "styled-components";
import {Color, FontSize} from "../config/Styles";
import {Languages} from "../functions/Languages";
import Navigation from "./Navigation";
import UserProfile from "./UserProfile";
import {Layout} from "../functions/Layout";
import NavigationMobile from "./NavigationMobile";
import {Modes} from "../functions/Modes";

const StyledHeaderWrap = styled.header`
    position: absolute;
    top: 0;
    width: 100%;
    min-width: 550px;
    height: 60px;
    background-color: ${Color.Basic};
    z-index: 2000;
    @media only screen and (max-width: 1379px) {
        position: fixed;
    }
    @media only screen and (max-device-width: 720px) {
        height: 90px;
    }
`;

// const StyledGradient = styled.div`
//     width: 100%;
//     height: 50px;
//     background: linear-gradient(to bottom, rgba(255,255,255,1), rgba(255,255,255,0));
// `;

const StyledHeader = styled.header`
    padding: 10px 20px;
    @media only screen and (max-device-width: 720px) {
        padding: 30px 5%;
    }
`;

interface IHeaderProps {
    layout: Layout;
    actualLanguage: Languages;
    setActualLanguage: (newLanguage: Languages) => void;
    setActualMode: (newMode: Modes) => void;
    isLogin: boolean;
    toggleIsLogin: () => void;
    isOpenBirthday: boolean;
    toggleIsOpenBirthday: () => void;
    isOpenNote: boolean;
    toggleIsOpenNote: () => void;
    isOpenMenuBox: boolean;
    toggleIsOpenMenuBox: () => void;
}

class Header extends React.Component<IHeaderProps, {}> {
    public render() {
        const {layout, actualLanguage, setActualLanguage, setActualMode, isLogin, toggleIsLogin, isOpenBirthday, toggleIsOpenBirthday, isOpenNote, toggleIsOpenNote, isOpenMenuBox, toggleIsOpenMenuBox} = this.props;
        return (
            <StyledHeaderWrap>
                <StyledHeader>
                {/*Vykreslenie hlavicky uzivatela v stave LOG IN*/}
                {isLogin &&
                    (<UserProfile
                        toggleIsLogin={toggleIsLogin}
                        isOpenBirthday={isOpenBirthday}
                        toggleIsOpenBirthday={toggleIsOpenBirthday}
                        isOpenNote={isOpenNote}
                        toggleIsOpenNote={toggleIsOpenNote}
                        setActualMode={setActualMode}
                    />)
                }
                    <Navigation
                        actualLanguage={actualLanguage}
                        setActualLanguage={setActualLanguage}
                        isLogin={isLogin}
                        toggleIsLogin={toggleIsLogin}
                        isOpenBirthday={isOpenBirthday}
                        toggleIsOpenBirthday={toggleIsOpenBirthday}
                        isOpenNote={isOpenNote}
                        toggleIsOpenNote={toggleIsOpenNote}
                    />
                </StyledHeader>
                {/*<StyledGradient/>*/}
            </StyledHeaderWrap>
        );
    }
}

export default Header;
