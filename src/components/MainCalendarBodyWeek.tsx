import * as React from "react";
import {FormattedMessage} from "react-intl";
import styled from "styled-components";
import {Color} from "../config/Styles";
import WeekIssueBox from "./WeekIssueBox";
import BirthdayBoxWeek from "./BirthdayBoxWeek";

const StyledDayCell = styled.div`
    width: ${100 / 7}%;
    min-height: 500px;
    display: inline-block;
    border-right: 1px solid ${Color.Color1};
    border-bottom: 1px solid ${Color.Color1};
    &:first-child{
        border-left: 1px solid ${Color.Color1};
    }
    box-sizing: border-box;
    @media only screen and (max-device-width: 720px) {
            height: 700px;
        }
`;

class MainCalendarBodyWeek extends React.Component <{}, {}> {
    public render() {
        const dayCells = [];
        for (let i = 0; i < 7; i++) {
            dayCells.push(
                <StyledDayCell key={i}>
                    <BirthdayBoxWeek>
                        <FormattedMessage id={"birthday"}/>
                    </BirthdayBoxWeek>
                    <WeekIssueBox/>
                </StyledDayCell>,
            );
        }
        return(
            <div>{dayCells}</div>
        );
    }
}

export default MainCalendarBodyWeek;
