import * as React from "react";
import {FormattedMessage} from "react-intl";
import styled from "styled-components";
import {Color, FontSize, FontWeight} from "../config/Styles";

const StyledWeekIssueBox = styled.div`
    border-top: 1px solid ${Color.Color1};
    box-sizing: border-box;
    font-size: ${FontSize.Font3};
    @media only screen and (max-width: 720px) {
        font-size: ${FontSize.Font4};
    }
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font2};
    }
`;

const StyledIssueTime = styled.div`
    padding: 5px;
    color: ${Color.Color4};
`;

const StyledIssueName = styled.div`
    padding: 5px;
    background-color: ${Color.Color1};
    color: ${Color.Basic};
`;

class WeekIssueBox extends React.Component <{}, {}> {
    public render() {
        return(
            <StyledWeekIssueBox>
                <StyledIssueTime>
                    <FormattedMessage id={"issueTimeMessage"} defaultMessage={"event time"}/>
                </StyledIssueTime>
                <StyledIssueName>
                    <FormattedMessage id={"issueNameMessage"} defaultMessage={"event names"}/>
                </StyledIssueName>
            </StyledWeekIssueBox>
        );
    }
}

export default WeekIssueBox;
