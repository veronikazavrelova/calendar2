import * as React from "react";
import styled from "styled-components";

enum IconImage {
    birthday,
    note,
    menuBox,
    login,
}

enum BackgroundSize {
    cover,
    contain,
}

interface IStyledIconProps {
    iconImage: IconImage;
    isActive: boolean;
    backgroundSize: BackgroundSize;
}

const StyledIcon = styled.div`
    width: 40px;
    height: 28px;
    background-image: ${(props: IStyledIconProps) => {
        switch (props.iconImage) {
            case IconImage.birthday:
                return props.isActive ? "url('src/images/birthdayActive.svg')" : "url('src/images/birthday.svg')";
            case IconImage.note:
                return props.isActive ? "url('src/images/noteActive.svg')" : "url('src/images/note.svg')";
            case IconImage.login:
                return props.isActive ? "url('src/images/personActive.svg')" : "url('src/images/person.svg')";
        }
    }};
    &:hover {
        background-image: ${(props: IStyledIconProps) => {
            switch (props.iconImage) {
                case IconImage.birthday:
                    return "url('src/images/birthdayActive.svg')";
                case IconImage.note:
                    return "url('src/images/noteActive.svg')";
                case IconImage.login:
                    return "url('src/images/personActive.svg')";
            }
        }};
    }
    background-repeat: no-repeat;
    background-size: ${(props: IStyledIconProps) => {
        switch (props.backgroundSize) {
            case BackgroundSize.contain:
                return "contain";
            case BackgroundSize.cover:
                return "cover";
        }
    }};
    @media (max-device-width: 720px) {
        width: 60px;
        height: 40px;
    }
`;

interface IIconProps {
    iconImage: IconImage;
    onClick?: () => void;
    isActive?: boolean;
    backgroundSize?: BackgroundSize;
}

class Icon extends React.Component<IIconProps, {}> {
    public static defaultProps: Partial<IIconProps> = {
        isActive: false,
        backgroundSize: BackgroundSize.cover,
    };
    public render() {
        const {iconImage, onClick, isActive, backgroundSize} = this.props;
        return (
            <StyledIcon iconImage={iconImage} onClick={onClick} isActive={isActive} backgroundSize={backgroundSize}/>
        );
    }
}

export default Icon;
export {IconImage, BackgroundSize};
