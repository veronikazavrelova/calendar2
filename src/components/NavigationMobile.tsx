import * as React from "react";
import {FormattedMessage} from "react-intl";
import Icon, {BackgroundSize, IconImage} from "./Icon";
import styled from "styled-components";
import NavigationMobileItem from "./NavigationMobileItem";

const StyledNavigationItem = styled.div`
    float: right;
`;

interface INavigationMobile {
    isOpenMenuBox: boolean;
    toggleIsOpenMenuBox: () => void;
}

class NavigationMobile extends React.Component <INavigationMobile, {}> {
    public render() {
        const {isOpenMenuBox, toggleIsOpenMenuBox} = this.props;
        return (
            <StyledNavigationItem>
                <Icon
                    iconImage={IconImage.menuBox}
                    isActive={isOpenMenuBox}
                    backgroundSize={BackgroundSize.contain}
                    onClick={() => {
                        toggleIsOpenMenuBox();
                    }}
                />
                {isOpenMenuBox &&
                    <NavigationMobileItem>
                        <FormattedMessage id={"birthDate"} defaultMessage={"date od birth"}/>
                    </NavigationMobileItem>
                }
            </StyledNavigationItem>
        );
    }
}

export default NavigationMobile;
