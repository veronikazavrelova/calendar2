import * as React from "react";
import styled from "styled-components";
import {Color, FontSize, FontWeight} from "../config/Styles";
import {drawIssues} from "../functions/dateFunctions";
import {initHolidays} from "../functions/Holidays";
import {Languages} from "../functions/Languages";
import {names} from "../functions/Names";

const StyledWeekDay = styled.div`
    background-color: ${Color.Color1};
    color: ${Color.Basic};
    font-size: ${FontSize.Font2};
    @media only screen and (max-width: 770px) {
        font-size: ${FontSize.Font3};
    }
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font5};
    }
`;

const StyledDayName = styled.div`
    text-align: center;
    padding: 0 10px;
`;

const StyledCellWrap = styled.div`
    font-weight: ${FontWeight.Thin};
    position: relative;
`;

const StyledCellName = styled.div`
    padding: 0 10px;
    display: inline-block;
`;

const StyledCellHolidays = StyledCellName.extend`
    position: absolute;
    right: 0;
`;

const StyledNames = styled.div`
    text-align: center;
    padding: 0 10px;
`;

interface IWeekOneDayProps {
    dayNumber: number;
    dayName: JSX.Element;
    actualLanguage: Languages;
    actualDate: Date;
}

class WeekOneDay extends React.Component <IWeekOneDayProps, {}> {
    public render() {
        const {dayNumber, dayName, actualLanguage, actualDate} = this.props;
        // Podmienka pre zistenie existencie sviatka
        const isHoliday =
            initHolidays(actualDate)[actualLanguage][actualDate.getMonth()]
            && initHolidays(actualDate)[actualLanguage][actualDate.getMonth()][actualDate.getDate()];
        let cellWrap;
        if (isHoliday) {
            cellWrap = (
                <StyledCellWrap>
                    <StyledCellName>
                        {drawIssues(names, actualLanguage, actualDate)}
                    </StyledCellName>
                    <StyledCellHolidays>
                    {drawIssues(initHolidays(actualDate), actualLanguage, actualDate)}
                    </StyledCellHolidays>
                </StyledCellWrap>
            );
        } else {
            cellWrap = (
                <StyledNames>
                    {drawIssues(names, actualLanguage, actualDate)}
                </StyledNames>
            );
        }
        return(
            <StyledWeekDay>
                <StyledDayName>
                    {dayNumber}{". "}{dayName}
                </StyledDayName>
                {cellWrap}
            </StyledWeekDay>
        );
    }
}

export default WeekOneDay;
