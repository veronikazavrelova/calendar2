import * as React from "react";
import styled from "styled-components";
import {Color, FontSize} from "../config/Styles";

const StyledList = styled.ul`
    width: 100%;
    height: 100%;
    position: fixed;
    margin-top: 10px;
    background-color: ${Color.ColorDefaultOp2};
    left: 0;
`;
const StyledListItem = styled.li`
    padding: 10px 10px 0 10px;
    &:last-child {
        padding-bottom: 10px;
    }
    border-bottom: 1px solid ${Color.Basic};
    color: ${Color.Basic};
    font-size: ${FontSize.Font3};

`;
class NavigationMobileItem extends React.Component <{}, {}> {
    public render() {
        return(
            <StyledList>
                <StyledListItem>{this.props.children}</StyledListItem>
            </StyledList>
        );
    }
}

export default NavigationMobileItem;
