import * as React from "react";
import {FormattedMessage} from "react-intl";
import styled from "styled-components";
import BirthdayBox from "./BirthdayBox";
import DayIssueBox from "./DayIssueBox";
import {Color, FontSize} from "../config/Styles";

const StyledMainCalendarBodyDay = styled.div`
    width: 100%;
    height: 500px;
    position: relative;
    border: 1px solid ${Color.Color1};
    box-sizing: border-box;
    @media only screen and (max-device-width: 720px) {
        height: auto;
        min-height: 700px;
    }
`;

const StyledBirthdayBoxScroll = styled.div`
    overflow: auto;
    white-space: nowrap;
    @media only screen and (max-device-width: 720px) {
        width: 90%;
        margin: 10px auto;
        overflow: initial;
        white-space: initial;
    }
`;

const StyledBirthdayBoxWrap = styled.div`
    display: inline-block;
    margin: 10px 5px 5px 0;
    &:first-child {
        margin-left: 10px;
    };
    &:last-child {
        margin-right: 10px;
    };
    &:first-child {
        margin-left: 10px;
    }
    @media only screen and (max-device-width: 720px) {
        margin: 10px 0;
        margin-right: 10px;
        &:first-child {
            margin-left: 0;
    };
    }
`;

const StyledDayIssueBoxWrap = StyledBirthdayBoxScroll.extend`
    @media only screen and (max-device-width: 720px) {
        width: 100%;
    }
`;

const StyledAddNote = styled.div`
    width: 90%;
    margin: 20px 5%;
    position: absolute;
    bottom: 0;
    text-align: right;
    font-size: ${FontSize.Font2};
    cursor: pointer;
    &:hover {
        color: ${Color.Color4};
    }
    @media only screen and (max-device-width: 720px) {
        position: relative;
        font-size: ${FontSize.Font1};
    }
`;

interface IMainCalendarBodyDayProps {
    isLogin: boolean;
    toggleIsOpenNote: () => void;
}

class MainCalendarBodyDay extends React.Component <IMainCalendarBodyDayProps, {}> {
    public render() {
        const {isLogin, toggleIsOpenNote} = this.props;
        return (
            <StyledMainCalendarBodyDay>
                <StyledBirthdayBoxScroll>
                    <StyledBirthdayBoxWrap>
                        <BirthdayBox>
                            <FormattedMessage id={"birthday"}/>
                        </BirthdayBox>
                    </StyledBirthdayBoxWrap>
                </StyledBirthdayBoxScroll>
                <StyledDayIssueBoxWrap>
                    <DayIssueBox/>
                    <DayIssueBox/>
                    <DayIssueBox/>
                    <DayIssueBox/>
                    <DayIssueBox/>
                    <DayIssueBox/>
                    <DayIssueBox/>
                </StyledDayIssueBoxWrap>
                {isLogin &&
                    <StyledAddNote
                        onClick={() => {
                            toggleIsOpenNote();
                        }}
                    >
                        {"+ "}
                        <FormattedMessage id={"addNote"} defaultMessage={"add reminder"}/>
                    </StyledAddNote>
                }
            </StyledMainCalendarBodyDay>
        );
    }
}

export default MainCalendarBodyDay;
