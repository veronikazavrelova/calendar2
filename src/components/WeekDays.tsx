import * as React from "react";
import {FormattedMessage} from "react-intl";
import styled from "styled-components";
import {Color, FontFamily, FontSize, FontWeight, LetterSpacing} from "../config/Styles";
import {getWeekNumber} from "../functions/dateFunctions";
import {weekDaysShort} from "../functions/translate";

const StyledWrap = styled.div`
    line-height: initial;
`;

const StyledWeekDay = styled.div`
    width: ${100 / 7}%;
    display: inline-block;
    background-color: ${Color.Color1};
    border-right: 1px solid ${Color.Color1};
    box-sizing: border-box;
    color: ${Color.Basic};
    font-size: ${FontSize.Font2};
    line-height: 30px;
    text-align: center;
    @media only screen and (max-width: 770px) {
        font-size: ${FontSize.Font3};
    }
    @media only screen and (max-device-width: 720px) {
        padding: 10px;
        font-size: ${FontSize.Font5};
    }
`;

const StyledWeekNumber = styled.div`
    padding: 5px 25px;
    border: 1px solid ${Color.Color1};
    background-color: ${Color.Basic};
    color: ${Color.Color1};
    font-size: ${FontSize.Font3};
    text-align: right;
    @media (max-device-width: 720px) {
        padding: 10px 25px;
        font-size: ${FontSize.Font2};
    }
`;

interface IWeekDaysProps {
    actualDate: Date;
}

class WeekDays extends React.Component <IWeekDaysProps, {}> {
    public render() {
        const dayNames = [];
        let actualDrawDate = new Date(this.props.actualDate.getTime());
        for (let i = 0; i < 7; i++) {
            dayNames.push(
                <StyledWeekDay key={i}>{actualDrawDate.getDate()}{". "}{weekDaysShort[i]}</StyledWeekDay>,
            );
            actualDrawDate.setDate(actualDrawDate.getDate() + 1);
        }
        actualDrawDate = new Date(this.props.actualDate.getTime());
        return(
            <StyledWrap>
                {dayNames}
                <StyledWeekNumber>
                    {getWeekNumber(actualDrawDate)}{". "}
                    <FormattedMessage id={"week"}/>
                </StyledWeekNumber>
            </StyledWrap>
        );
    }
}

export default WeekDays;
