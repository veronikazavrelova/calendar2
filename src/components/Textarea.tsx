import * as React from "react";
import styled from "styled-components";
import {Color, FontSize, FontWeight} from "../config/Styles";
import {TextareaHTMLAttributes} from "react";

const StyledLabel = styled.label`
    display: block;
    padding-bottom: 10px;
    font-size: ${FontSize.Font3};
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font6};
    }
`;

const StyledtTextarea = styled.textarea`
    resize: none;
    width: 90%;
    height: 120px;
    padding: 5px 5%;
    border: 1px solid ${Color.Color2};
    font-size: ${FontSize.Font3};
    font-weight: ${FontWeight.Thin};
    &::placeholder {
        color: ${Color.Color2};
    }
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font6};
        height: 500px;
    }
`;

class Textarea extends React.Component <TextareaHTMLAttributes<HTMLTextAreaElement>, {}> {
    public render() {
        const {children, ...props} = this.props;
        return(
            <div>
                <StyledLabel>
                    {children}
                </StyledLabel>
                <StyledtTextarea {...props}/>
            </div>
        );
    }
}

export default Textarea;
