import {DatePicker} from "antd";
import TimePicker from "antd/es/time-picker";
import * as React from "react";
import {FormattedMessage, InjectedIntlProps, injectIntl} from "react-intl";
import styled from "styled-components";
import {Color, FontSize} from "../config/Styles";
import Button, {ButtonFontSize, ButtonPadding} from "./Button";
import Input from "./Input";
import Textarea from "./Textarea";

const StyledWrap = styled.div`
    margin: auto;
    margin-top: 20px;
    width: 90%;
    @media only screen and (max-device-width: 720px) {
        margin-top: 40px;
    }
`;

const StyledSpan = styled.span`
    display: block;
`;

const StyledLabel = styled.div`
    padding-bottom: 10px;
    font-size: ${FontSize.Font3};
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font6};
    }
`;

const StyledButtonWrap = styled.div`
    margin: 20px 5%;
    text-align: right;
    @media only screen and (max-device-width: 720px) {
        margin: 40px 5%;
    }
`;

const FormNote = injectIntl<{}>(class extends React.Component <InjectedIntlProps, {}> {
    public render() {
        const formatTime = "HH:mm";
        const formatDate = "DD-MM-YYYY"
        return(
            <form>
                {/*datepicker pre datum*/}
                <StyledWrap>
                    <StyledLabel>
                        <FormattedMessage id={"NotificationDate"} defaultMessage={"date of reminder"}/>:
                    </StyledLabel>
                    <DatePicker format={formatDate} placeholder={"__-__-____"}/>
                </StyledWrap>
                {/*input pre cas*/}
                <StyledWrap>
                    <StyledSpan>
                        <StyledLabel>
                            <FormattedMessage id={"timeBeginn"} defaultMessage={"beginn of reminder"}/>:
                        </StyledLabel>
                        <TimePicker format={formatTime} placeholder={"00:00"}/>
                    </StyledSpan>
                </StyledWrap>
                <StyledWrap>
                    <StyledSpan>
                        <StyledLabel>
                            <FormattedMessage id={"timeEnd"} defaultMessage={"end of reminder"}/>:
                        </StyledLabel>
                        <TimePicker format={formatTime} placeholder={"00:00"}/>
                    </StyledSpan>
                </StyledWrap>
                {/*input pre nazov*/}
                <StyledWrap>
                    <Input
                        maxLength={20}
                        placeholder={this.props.intl.formatMessage({
                            id: "NotificationName",
                            defaultMessage: "name of reminder",
                        })}
                    >
                        <FormattedMessage id={"NotificationName"} defaultMessage={"name of reminder"}/>:
                    </Input>
                </StyledWrap>
                {/*textarea pre popis*/}
                <StyledWrap>
                    <Textarea
                        placeholder={this.props.intl.formatMessage({
                            id: "NotificationText",
                            defaultMessage: "description of reminder",
                        })}
                    >
                            <FormattedMessage id={"NotificationText"} defaultMessage={"description of reminder"}/>:
                    </Textarea>
                </StyledWrap>
                {/*tlacidlo pre ulozenie*/}
                <StyledButtonWrap>
                    <Button type={"submit"} fontSize={ButtonFontSize.Submit} buttonPadding={ButtonPadding.Submit}>
                        <FormattedMessage id={"save"}/>
                    </Button>
                </StyledButtonWrap>
            </form>
        );
    }
    // public setInputTimeValue(event) {
    //     let value = event.target.value;
    //     this.setState((oldState) => {
    //         switch (value.length) {
    //             case 0:
    //                 break;
    //             case 1:
    //                 value = value.match(/[0-2]/) ? value : oldState.inputTimeValue;
    //                 break;
    //             case 2:
    //                 value = value.match(/([0-1][0-9]|2[0-3])/) ? value + (oldState.inputTimeValue.length === 1 ? ":" : "") : oldState.inputTimeValue;
    //                 break;
    //             case 3:
    //                 value = value.match(/([0-1][0-9]|2[0-3]):/) ? value : oldState.inputTimeValue;
    //                 break;
    //             case 4:
    //                 value = value.match(/([0-1][0-9]|2[0-3]):[0-5]/) ? value : oldState.inputTimeValue;
    //                 break;
    //             case 5:
    //                 value = value.match(/([0-1][0-9]|2[0-3]):[0-5][0-9]/) ? value : oldState.inputTimeValue;
    //                 break;
    //             default:
    //                 value = oldState.inputTimeValue;
    //         }
    //         return {
    //             inputTimeValue: value,
    //             // inputTimeValue: value === "" || value.match(/^([0-2]|([0-1][0-9]|2[0-3])|([0-1][0-9]|2[0-3]):|([0-1][0-9]|2[0-3]):[0-5]|([0-1][0-9]|2[0-3]):[0-5][0-9])$/) ? value : oldState.inputTimeValue,
    //         };
    //     });
    // }
});

export default FormNote;
