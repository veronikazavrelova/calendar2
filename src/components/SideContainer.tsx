import * as React from "react";
import styled from "styled-components";
import {minusDrawYear, plusDrawYear} from "../functions/dateFunctions";
import SideCalendar from "./SideCalendar";
import {months} from "../functions/translate";
import {Modes} from "../functions/Modes";

const StyledSideContainer = styled.span`
    width: 247px;
    padding-right: 20px;
    float: left;
`;

interface ISideContainerProps {
    actualDate: Date;
    actualMode: Modes;
}

class SideContainer extends React.Component<ISideContainerProps, {}> {
    public render() {
        const {actualMode, actualDate} = this.props;
        const plusYear = plusDrawYear(actualDate.getMonth(), actualDate.getFullYear());
        const minusYear = minusDrawYear(actualDate.getMonth(), actualDate.getFullYear());
        return (
            <StyledSideContainer>
                <SideCalendar
                    month={actualMode === Modes.MONTH ? minusYear[0] : actualDate.getMonth()}
                    year={actualMode === Modes.MONTH ? minusYear[1] : actualDate.getFullYear()}
                    actualDate={actualDate}
                    actualMode={actualMode}
                />
                {/*Vykresli sa IBA v Mode.MONTH*/}
                {actualMode === Modes.MONTH &&
                    (<SideCalendar
                        month={plusYear[0]}
                        year={plusYear[1]}
                        actualDate={actualDate}
                        actualMode={actualMode}
                    />)
                }
            </StyledSideContainer>
        );
    }
}

export default SideContainer;
