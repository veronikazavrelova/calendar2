import * as React from "react";
import styled from "styled-components";
import {Color, FontFamily, FontSize, FontWeight, LetterSpacing} from "../config/Styles";
import {languages} from "../functions/translate";
import {Languages} from "../functions/Languages";

const StyledLanguagesList = styled.ul`
    padding: 0;
`;

const StyledSublistItem = styled.li`
    padding: 5px;
    background-color: ${Color.Color1};
    color: ${Color.Basic};
    font-size: ${FontSize.Font2};
    text-align: center;
    line-height: 18px;
    &:hover{
        background-color: ${Color.Basic};
        color: ${Color.Color1};
    }
    @media (max-width: 720px) {
        font-size: ${FontSize.Font3};
    }
    @media (max-device-width: 720px) {
        font-size: ${FontSize.Font5};
        padding: 10px;
    }
`;

interface ILanguagesListProps {
    setActualLanguage: (newLanguage: Languages) => void;
}

class LanguagesList extends React.Component<ILanguagesListProps, {}> {
    public render() {
        const items = [];
        for (const key in languages) {
            items.push(
                <StyledSublistItem key={key} onClick={() => this.props.setActualLanguage(key as any)}>
                    {languages[key]}
                </StyledSublistItem>,
            );
        }
        return (
            <StyledLanguagesList>
                {items}
            </StyledLanguagesList>
        );
    }
}

export default LanguagesList;
