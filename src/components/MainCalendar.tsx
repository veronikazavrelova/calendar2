import * as React from "react";
import styled from "styled-components";
import {Color, Shadow} from "../config/Styles";
import {convertDayIndex} from "../functions/dateFunctions";
import {Languages} from "../functions/Languages";
import {Modes} from "../functions/Modes";
import {weakDays} from "../functions/translate";
import MainCalendarBodyDay from "./MainCalendarBodyDay";
import MainCalendarBodyMonth from "./MainCalendarBodyMonth";
import MainCalendarBodyWeek from "./MainCalendarBodyWeek";
import MainCalendarHead from "./MainCalendarHead";
import Week from "./Week";
import WeekDays from "./WeekDays";
import WeekOneDay from "./WeekOneDay";
import {Layout} from "../functions/Layout";

const StyledMainCalendar = styled.div`
    width: 700px;
    display: inline-block;
    box-shadow: ${Shadow.BoxShadow};
    background-color: ${Color.Basic};
    margin-right: 20px;
    margin-bottom: 20px;
    @media only screen and (max-width: 720px) {
        width: 95%;
        margin: auto;
        margin-bottom: 20px;
        display: block;
    }
    @media only screen and (min-width: 721px) and (max-width: 1379px) {
        width: 700px;
        margin: auto;
        margin-bottom: 20px;
        display: block;
    }
    @media only screen and (max-device-width: 720px) {
        width: 95%;
        margin: auto;
        margin-bottom: 20px;
        display: block;
    }
`;

interface IMainCalendarProps {
    layout: Layout;
    actualDate: Date;
    actualMode: Modes;
    setActualDate: (newDate: Date) => void;
    setActualMode: (newMode: Modes) => void;
    actualLanguage: Languages;
    isLogin: boolean;
    isOpenBirthday: boolean;
    isOpenNote: boolean;
    toggleIsOpenNote: () => void;
}

class MainCalendar extends React.Component <IMainCalendarProps, {}> {
    public render() {
        const {layout, actualDate, actualMode, setActualDate, setActualMode, actualLanguage, isLogin, isOpenBirthday, isOpenNote, toggleIsOpenNote} = this.props;
        const CalendarBody = [];
        {/*Vykreslenie hlavneho kalendara, ak je aktivny mod MONTH*/}
        if (actualMode === Modes.MONTH) {
            CalendarBody.push(
                <span key={"0"}>
                    <Week/>
                    <MainCalendarBodyMonth
                        year={actualDate.getFullYear()}
                        month={actualDate.getMonth()}
                        actualLanguage={actualLanguage}
                        actualDate={actualDate}
                        setActualMode={setActualMode}
                        setActualDate={setActualDate}
                        isOpenBirthday={isOpenBirthday}
                        isLogin={isLogin}
                        isOpenNote={isOpenNote}
                        layout={layout}
                    />
                </span>,
            );
            {/*Vykreslenie hlavneho kalendara, ak je aktivny mod DAY*/}
        } else if (actualMode === Modes.DAY) {
            CalendarBody.push(
                <span key={"1"}>
                    <WeekOneDay
                        dayNumber={actualDate.getDate()}
                        dayName={weakDays[convertDayIndex(actualDate.getDay())]}
                        actualLanguage={this.props.actualLanguage}
                        actualDate={actualDate}
                    />
                    <MainCalendarBodyDay
                        isLogin={isLogin}
                        toggleIsOpenNote={toggleIsOpenNote}
                    />
                </span>,
            );
            {/*Vykreslenie hlavneho kalendara, ak je aktivny mod WEEK*/}
        } else if (actualMode === Modes.WEEK) {
            CalendarBody.push(
                <span key={"2"}>
                    <WeekDays actualDate={actualDate}/>
                    <MainCalendarBodyWeek/>
                </span>,
            );
        }
        return (
            <StyledMainCalendar>
                <MainCalendarHead
                    actualDate={actualDate}
                    actualMode={actualMode}
                    setActualDate={setActualDate}
                    setActualMode={setActualMode}
                    year={actualDate.getFullYear()}
                />
                <div>
                    {CalendarBody}
                </div>
            </StyledMainCalendar>
        );
    }
}

export default MainCalendar;
