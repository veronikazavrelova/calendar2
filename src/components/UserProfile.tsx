import * as React from "react";
import {FormattedMessage} from "react-intl";
import styled from "styled-components";
import {Color, FontSize} from "../config/Styles";
import Button from "./Button";
import {Modes} from "../functions/Modes";

const StyledUserProfile = styled.div`
    display: inline-block;
    line-height: 40px;
`;

const StyledUserPhoto = styled.div`
    width: 30px;
    height: 30px;
    margin: 5px 0;
    display: inline-block;
    position: relative;
    top: -2px;
    vertical-align: middle;
    border-radius: 50%;
    border: 1px solid ${Color.Color1};
    background-image: url("src/images/ninka.jpg");
    background-size: cover;
    @media (max-device-width: 720px) {
        width: 40px;
        height: 40px;
    }
`;

const StyledUserName = styled.div`
    display: inline-block;
    padding-left: 10px;
    color: ${Color.Color2};
    font-size: ${FontSize.Font2};
    ${StyledUserProfile}:hover & {
        visibility: hidden;
    }
     @media (max-device-width: 720px) {
        font-size: ${FontSize.Font5};
    }
`;

const StyledLogOut = styled.div`
    display: none;
    position: fixed;
    left: 70px;
    ${StyledUserProfile}:hover & {
        display: initial;
    }
`;

interface IUserProfileProps {
    setActualMode: (newMode: Modes) => void;
    toggleIsLogin: () => void;
    isOpenBirthday: boolean;
    toggleIsOpenBirthday: () => void;
    isOpenNote: boolean;
    toggleIsOpenNote: () => void;
}

class UserProfile extends React.Component <IUserProfileProps, {}> {
    public render() {
        const {setActualMode, toggleIsLogin, isOpenBirthday, toggleIsOpenBirthday, isOpenNote, toggleIsOpenNote} = this.props;
        return(
            <StyledUserProfile>
                <StyledUserPhoto/>
                <StyledUserName>
                    {"Veronika Zavrelova"}
                </StyledUserName>
                <StyledLogOut>
                    <Button
                        onClick={() => {
                            toggleIsLogin();
                            setActualMode(Modes.MONTH)
                            isOpenBirthday && toggleIsOpenBirthday();
                            isOpenNote && toggleIsOpenNote();
                        }}
                    >
                        <FormattedMessage id={"logOut"} defaultMessage={"Log out"}/>
                    </Button>
                </StyledLogOut>
            </StyledUserProfile>
        );
    }
}

export default UserProfile;
