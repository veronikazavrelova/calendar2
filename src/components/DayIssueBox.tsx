import * as React from "react";
import {FormattedMessage} from "react-intl";
import styled from "styled-components";
import {Color, FontSize, Shadow} from "../config/Styles";

const StyledDayIssueBox = styled.div`
    width: 160px;
    margin: 5px 10px;
    background-color: ${Color.Basic};
    display: inline-block;
    box-shadow: ${Shadow.BoxShadow};
    @media (max-device-width: 720px) {
        width: 90%;
        margin: 20px 5%;
    }
`;

const StyledIssueTime = styled.div`
    padding: 5px;
    color: ${Color.Color4};
    font-size: ${FontSize.Font3};
    font-weight: bold;
    @media only screen and (max-device-width: 720px) {
        padding: 10px 20px;
        font-size: ${FontSize.Font1};
    }
`;

const StyledIssueName = styled.div`
    padding: 5px;
    background-color: ${Color.Color1};
    color: ${Color.Basic};
    font-size: ${FontSize.Font3};
    @media only screen and (max-device-width: 720px) {
        padding: 10px 20px;
        font-size: ${FontSize.Font1};
    }
`;

const StyledIssue = styled.div`
    height: 150px;
    padding: 5px;
    color: ${Color.Color4};
    font-size: ${FontSize.Font3};
    overflow: auto;
    white-space: normal;
    @media only screen and (max-device-width: 720px) {
        padding: 10px 20px;
        font-size: ${FontSize.Font5};
        height: 500px;
    }
`;

class DayIssueBox extends React.Component <{}, {}> {
    public render() {
        return(
            <StyledDayIssueBox>
                <StyledIssueTime>
                    <FormattedMessage id={"issueTimeMessage"} defaultMessage={"event time"}/>
                </StyledIssueTime>
                <StyledIssueName>
                    <FormattedMessage id={"issueNameMessage"} defaultMessage={"event names"}/>
                </StyledIssueName>
                <StyledIssue>
                    <FormattedMessage id={"issueMessage"} defaultMessage={"Here you will see a description of your reminder."}/>
                </StyledIssue>
            </StyledDayIssueBox>
        );
    }
}

export default DayIssueBox;
