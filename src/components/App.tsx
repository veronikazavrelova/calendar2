import * as React from "react";
import {addLocaleData, IntlProvider} from "react-intl";
import * as skLocale from "react-intl/locale-data/sk";
import styled from "styled-components";
import {Color} from "../config/Styles";
import {Languages} from "../functions/Languages";
import {Layout} from "../functions/Layout";
import {Modes} from "../functions/Modes";
import cz from "../locales/cz";
import sk from "../locales/sk";
import Header from "./Header";
import MainContainer from "./MainContainer";
import CreateNotification from "./CreateNotification";
import CreateBirthday from "./CreateBirthday";

addLocaleData(skLocale);

const StyledWrapContainer = styled.div`
    width: 100%;
    min-width: 550px;
    background-color: ${Color.Basic};
    @media (max-width: 720px) {
        position: absolute;
    }
`;

const StyledFlowersL = styled.img`
    height: 250px;
    position: fixed;
    left: 0;
    bottom: 0;
    z-index: -100;
`;

const StyledFlowersR = styled.img`
    height: 250px;
    position: fixed;
    right: 0;
    bottom: 0;
    z-index: -101;
`;

interface IAppState {
    layout: Layout;
    actualDate: Date;
    actualLanguage: Languages;
    actualMode: Modes;
    isLogin: boolean;
    isOpenNote: boolean;
    isOpenBirthday: boolean;
    isOpenMenuBox: boolean;
}

const messages = {
    [Languages.SK]: sk,
    [Languages.CZ]: cz,
};

class App extends React.Component<{}, IAppState> {
    constructor(props) {
        super(props);
        this.state = {
            layout: this.calculateLayout(),
            actualDate: new Date(),
            actualLanguage: Languages.SK,
            actualMode: Modes.MONTH,
            isLogin: false,
            isOpenBirthday: false,
            isOpenNote: false,
            isOpenMenuBox: false,
        };
        this.setActualLanguage = this.setActualLanguage.bind(this);
        this.setActualDate = this.setActualDate.bind(this);
        this.setActualMode = this.setActualMode.bind(this);
        this.toggleIsLogin = this.toggleIsLogin.bind(this);
        this.toggleIsOpenNote = this.toggleIsOpenNote.bind(this);
        this.toggleIsOpenBirthday = this.toggleIsOpenBirthday.bind(this);
        this.toggleIsOpenMenuBox = this.toggleIsOpenMenuBox.bind(this);

        window.onresize = () => {
            this.setState({layout: this.calculateLayout()});
        };
    }
    public render() {
        return (
            <IntlProvider locale="sk" messages={messages[this.state.actualLanguage]}>
                <StyledWrapContainer>
                    <Header
                        layout={this.state.layout}
                        actualLanguage={this.state.actualLanguage}
                        setActualLanguage={this.setActualLanguage}
                        setActualMode={this.setActualMode}
                        isLogin={this.state.isLogin}
                        toggleIsLogin={this.toggleIsLogin}
                        isOpenBirthday={this.state.isOpenBirthday}
                        toggleIsOpenBirthday={this.toggleIsOpenBirthday}
                        isOpenNote={this.state.isOpenNote}
                        toggleIsOpenNote={this.toggleIsOpenNote}
                        isOpenMenuBox={this.state.isOpenMenuBox}
                        toggleIsOpenMenuBox={this.toggleIsOpenMenuBox}
                    />
                    <MainContainer
                        layout={this.state.layout}
                        actualDate={this.state.actualDate}
                        actualMode={this.state.actualMode}
                        setActualDate={this.setActualDate}
                        setActualMode={this.setActualMode}
                        actualLanguage={this.state.actualLanguage}
                        isLogin={this.state.isLogin}
                        isOpenBirthday={this.state.isOpenBirthday}
                        toggleIsOpenBirthday={this.toggleIsOpenBirthday}
                        isOpenNote={this.state.isOpenNote}
                        toggleIsOpenNote={this.toggleIsOpenNote}
                    />
                    {this.state.layout !== Layout.DESKTOP && this.state.isOpenBirthday &&
                    <CreateBirthday
                        toggleIsOpenBirthday={this.toggleIsOpenBirthday}
                        actualLanguage={this.state.actualLanguage}
                    />
                    }
                    {this.state.layout !== Layout.DESKTOP && this.state.isOpenNote &&
                    <CreateNotification
                        toggleIsOpenNote={this.toggleIsOpenNote}
                    />
                    }
                    <StyledFlowersL src="src/images/flowersL.jpg"/>
                    <StyledFlowersR src="src/images/flowersR.jpg"/>
                </StyledWrapContainer>
            </IntlProvider>
        );
    }
    public setActualLanguage(newLanguage: Languages) {
        this.setState({actualLanguage: newLanguage});
    }
    public setActualDate(newDate: Date) {
        this.setState({actualDate: newDate});
    }
    public setActualMode(newMode: Modes) {
        this.setState({actualMode: newMode});
    }
    public toggleIsLogin() {
        this.setState((prev) => ({
            isLogin: !prev.isLogin,
        }));
    }
    public toggleIsOpenBirthday() {
        this.setState((prev) => ({
            isOpenBirthday: !prev.isOpenBirthday,
        }));
    }
    public toggleIsOpenNote() {
        this.setState((prev) => ({
            isOpenNote: !prev.isOpenNote,
        }));
    }
    public toggleIsOpenMenuBox() {
        this.setState((prev) => ({
            isOpenMenuBox: !prev.isOpenMenuBox,
        }));
    }
    private calculateLayout(): Layout {
        const w = window.innerWidth;
        return w < 720 ? Layout.MOBILE :
            (w < 1380 ? Layout.TABLET : Layout.DESKTOP);
    }
}

export default App;
