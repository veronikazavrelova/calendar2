import * as React from "react";
import styled from "styled-components";
import {FontSize, Shadow} from "../config/Styles";

enum BoxShadow {
    Default,
    BoxShadow,
}

interface IStyledNavigationItemProps {
    shadow: BoxShadow;
}

const StyledNavigationItem = styled.li `
    margin-left: 20px;
    display: inline-block;
    float: left;
    box-shadow: ${(props: IStyledNavigationItemProps) => {
    switch (props.shadow) {
        case BoxShadow.Default:
            return "none";
        case BoxShadow.BoxShadow:
            return Shadow.ButtonShadow;
    }
}
    };
    cursor: pointer;
    .hover{
        display: none;
    }
    &:hover .hover{
        display: initial;
        z-index: 1100;
    }
    z-index: 1100;
    @media (max-device-width: 720px) {
        margin-left: 30px;
    }
`;

interface INavigationItemProps {
    shadow?: BoxShadow;
}

class NavigationItem extends React.Component<INavigationItemProps, {}> {
    public static defaultProps: Partial<INavigationItemProps> = {
        shadow: BoxShadow.Default,
    };
    public render() {
        return (
                <StyledNavigationItem shadow={this.props.shadow}>
                    {this.props.children}
                </StyledNavigationItem>
        );
    }
}

export default NavigationItem;
export {BoxShadow};
