import * as React from "react";
import styled from "styled-components";
import {Color, FontSize, FontWeight} from "../config/Styles";

const StyledBirthdayBox = styled.div`
    padding: 5px;
    background-color: ${Color.Color3};
    color: ${Color.Basic};
    font-size: ${FontSize.Font3};
    @media (max-width: 720px) {
        font-size: ${FontSize.Font4};
    }
    @media (max-device-width: 720px) {
        padding: 10px;
        font-size: ${FontSize.Font1};
    }
`;

class BirthdayBox extends React.Component <{}, {}> {
    public render() {
        return(
            <StyledBirthdayBox>{this.props.children}</StyledBirthdayBox>
        );
    }
}

export default BirthdayBox;
