import * as React from "react";
import styled from "styled-components";
import {Color} from "../config/Styles";
import {Languages} from "../functions/Languages";
import {Modes} from "../functions/Modes";
import CreateBirthday from "./CreateBirthday";
import CreateNotification from "./CreateNotification";
import MainCalendar from "./MainCalendar";
import SideContainer from "./SideContainer";
import {Layout} from "../functions/Layout";
import SideCalendar from "./SideCalendar";
import {minusDrawYear, plusDrawYear} from "../functions/dateFunctions";

const StyledMainContainerWrap = styled.div`
    position: absolute;
    width: 100%;
`;

const StyledMainContainer = styled.div`
    position: relative;
    top: 70px;
    display: block;
    @media (max-width: 1379px) {
        width: 95%;
        margin: auto;
    }
    @media (min-width: 1380px) {
        margin-left: 20px;
    }
    @media (max-device-width: 720px) {
        top: 100px
    }
`;

interface IMainContainerProps {
    layout: Layout;
    actualDate: Date;
    setActualDate: (newDate: Date) => void;
    setActualMode: (newMode: Modes) => void;
    actualMode: Modes;
    actualLanguage: Languages;
    isLogin: boolean;
    isOpenBirthday: boolean;
    toggleIsOpenBirthday: () => void;
    isOpenNote: boolean;
    toggleIsOpenNote: () => void;
}

class MainContainer extends React.Component<IMainContainerProps, {}> {
    public render() {
        const {layout, actualDate, setActualDate, actualMode, setActualMode, actualLanguage, isLogin, isOpenNote, toggleIsOpenNote, isOpenBirthday, toggleIsOpenBirthday} = this.props;
        const plusYear = plusDrawYear(actualDate.getMonth(), actualDate.getFullYear());
        const minusYear = minusDrawYear(actualDate.getMonth(), actualDate.getFullYear());
        return (
            <StyledMainContainerWrap>
                <StyledMainContainer>
                   {layout === Layout.DESKTOP
                       ? <SideContainer actualDate={actualDate} actualMode={actualMode}/>
                       : <SideCalendar
                           month={actualMode === Modes.MONTH ? minusYear[0] : actualDate.getMonth()}
                           year={actualMode === Modes.MONTH ? minusYear[1] : actualDate.getFullYear()}
                           actualDate={actualDate}
                           actualMode={actualMode}
                       />
                   }
                   <MainCalendar
                       actualDate={actualDate}
                       actualMode={actualMode}
                       setActualDate={setActualDate}
                       setActualMode={setActualMode}
                       actualLanguage={actualLanguage}
                       isLogin={isLogin}
                       isOpenBirthday={isOpenBirthday}
                       isOpenNote={isOpenNote}
                       toggleIsOpenNote={toggleIsOpenNote}
                       layout={layout}
                   />
                    {layout === Layout.DESKTOP && isOpenBirthday &&
                        <CreateBirthday
                           toggleIsOpenBirthday={toggleIsOpenBirthday}
                           actualLanguage={actualLanguage}
                        />
                   }
                   {layout === Layout.DESKTOP && isOpenNote &&
                        <CreateNotification
                            toggleIsOpenNote={toggleIsOpenNote}
                        />
                   }
                   {layout !== Layout.DESKTOP &&
                    actualMode === Modes.MONTH &&
                       <SideCalendar
                           month={plusYear[0]}
                           year={plusYear[1]}
                           actualDate={actualDate}
                           actualMode={actualMode}
                       />
                   }
                </StyledMainContainer>
            </StyledMainContainerWrap>
       );
    }
}

export default MainContainer;
