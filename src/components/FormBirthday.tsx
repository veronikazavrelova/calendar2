import * as React from "react";
import {FormattedMessage, injectIntl, InjectedIntlProps} from "react-intl";
import styled from "styled-components";
import {Color, FontSize, FontWeight} from "../config/Styles";
import {Languages} from "../functions/Languages";
import {languages} from "../functions/translate";
import Button, {ButtonFontSize, ButtonPadding} from "./Button";
import Input, {InputType} from "./Input";
import Select from "antd/es/select";
import {getSelectNames} from "../functions/selectFunctions";
import {DatePicker} from "antd";

const StyledWrap = styled.div`
    margin: auto;
    margin-top: 20px;
    width: 90%;
    @media only screen and (max-device-width: 720px) {
        margin-top: 40px;
    }
`;

const StyledLabel = styled.div`
    padding-bottom: 10px;
    font-size: ${FontSize.Font3};
    @media only screen and (max-device-width: 720px) {
        font-size: ${FontSize.Font6};
    }
`;

const StyledButtonWrap = styled.div`
    margin: 20px 10px;
    text-align: right;
    @media only screen and (max-device-width: 720px) {
        margin: 40px 5%;
    }
`;

interface IFormBirthdayProps {
    actualLanguage: Languages;
}

interface IFormBirthdayState {
    selectedCountry: Languages;
    selectedName: string;
}

const FormBirthday = injectIntl<IFormBirthdayProps>(class extends React.Component <IFormBirthdayProps & InjectedIntlProps, IFormBirthdayState> {
    constructor(props) {
        super(props);
        this.state = {
            selectedCountry: props.actualLanguage,
            selectedName: this.props.intl.formatMessage({
                id: "chooseName",
                defaultMessage: "choose name",
            }),
        };
        this.setSelectedCountry = this.setSelectedCountry.bind(this);
        this.setSelectedName = this.setSelectedName.bind(this);
    }
    public render() {
        const Option = Select.Option;
        const optionsCountry = [];
        const countrySelectOptions = [];
        for (const key in languages) {
            countrySelectOptions.push(
                {value: key, label: languages[key]},
            );
            optionsCountry.push(
                <Option key={key} value={key}>{languages[key]}</Option>,
            );
        }
        const formatDate = "DD-MM-YYYY";
        return(
            <form>
                {/*input pre vyber krajiny*/}
                <StyledWrap>
                    <StyledLabel>
                        <FormattedMessage id={"country"}/>:
                    </StyledLabel>
                    <Select
                        value={this.state.selectedCountry}
                        onChange={this.setSelectedCountry}
                    >
                        {optionsCountry}
                    </Select>
                </StyledWrap>
                {/*input pre vyber mena*/}
                <StyledWrap>
                    <StyledLabel>
                        <FormattedMessage id={"name"}/>:
                    </StyledLabel>
                    <Select
                        showSearch={true}
                        value={this.state.selectedName}
                        onChange={this.setSelectedName}
                    >
                        {getSelectNames(this.state.selectedCountry).map((name) => {
                            return <Option key={name} value={name}>{name}</Option>;
                        })}
                    </Select>
                </StyledWrap>
                {/*input pre zadanie priezviska*/}
                <StyledWrap>
                    <Input
                        maxLength={20}
                        placeholder={this.props.intl.formatMessage({
                            id: "lastname",
                        })}
                    >
                        <FormattedMessage id={"lastname"}/>:
                    </Input>
                </StyledWrap>
                {/*input pre datum narodenia*/}
                <StyledWrap>
                    <StyledLabel>
                        <FormattedMessage id={"birthDate"} defaultMessage={"date od birth"}/>:
                    </StyledLabel>
                    <DatePicker format={formatDate} placeholder={"__-__-____"}/>
                </StyledWrap>
                {/*tlacidlo na ulozenie*/}
                <StyledButtonWrap>
                    <Button type={"submit"} fontSize={ButtonFontSize.Submit} buttonPadding={ButtonPadding.Submit}>
                        <FormattedMessage id={"save"}/>
                    </Button>
                </StyledButtonWrap>
            </form>
        );
    }
    public setSelectedCountry(newCountry) {
        this.setState({selectedCountry: newCountry});
    }
    public setSelectedName(newName) {
        this.setState({selectedName: newName});
    }
})

export default FormBirthday;
