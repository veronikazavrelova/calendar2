import * as React from "react";
import styled from "styled-components";
import {Color, FontSize} from "../config/Styles";

const StyledBirthdayBoxWeek = styled.div`
    padding: 5px;
    background-color: ${Color.Color3};
    color: ${Color.Basic};
    font-size: ${FontSize.Font3};
    @media only screen and (max-width: 720px) {
        font-size: ${FontSize.Font4};
    }
    @media only screen and (max-device-width: 720px) {
        padding: 10px;
        font-size: ${FontSize.Font2};
        overflow: hidden;
        text-overflow: ellipsis;
    }
`;

class BirthdayBoxWeek extends React.Component <{}, {}> {
    public render() {
        return(
            <StyledBirthdayBoxWeek>{this.props.children}</StyledBirthdayBoxWeek>
        );
    }
}

export default BirthdayBoxWeek;
