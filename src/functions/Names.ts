import czechNames from "../locales/czechNames";
import slovakNames from "../locales/slovakNames";
import {Languages} from "./Languages";

export const names = {
    [Languages.SK]: slovakNames,
    [Languages.CZ]: czechNames,
};
