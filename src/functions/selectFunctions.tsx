import compareFunction from "./compareFunction";
import {names} from "./Names";

export const getSelectNames = (language) => {
    const selectNames = [];
    for (const key in names[language]) {
        for (const key2 in names[language][key]) {
            for (const name of names[language][key][key2].split(", ")) {
                selectNames.push(name);
            }
        }
    }
    //  testovanie duplicity mien
    // const test = {};
    // selectNames.forEach((name) => {
    //     if (test[name]) {
    //         test[name]++;
    //     } else {
    //         test[name] = 1;
    //     }
    // });
    // const duplicate = {};
    // for (const key in test) {
    //     if (test[key] > 1) {
    //         duplicate[key] = test[key];
    //     }
    // }
    // window.console.log(duplicate);
    selectNames.sort((name1, name2) => {
        return compareFunction(name1, name2, 1);
    });
    return selectNames.filter((value, index, self) => {
        return self.indexOf(value) === index;
    });
};
