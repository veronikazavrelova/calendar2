import * as React from "react";
import {FormattedMessage} from "react-intl";
import {Languages} from "./Languages";

export const months = [
    <FormattedMessage id={"january"} key={0}/>,
    <FormattedMessage id={"february"} key={1}/>,
    <FormattedMessage id={"march"} key={2}/>,
    <FormattedMessage id={"april"} key={3}/>,
    <FormattedMessage id={"may"} key={4}/>,
    <FormattedMessage id={"juny"} key={5}/>,
    <FormattedMessage id={"july"} key={6}/>,
    <FormattedMessage id={"august"} key={7}/>,
    <FormattedMessage id={"september"} key={8}/>,
    <FormattedMessage id={"october"} key={9}/>,
    <FormattedMessage id={"november"} key={10}/>,
    <FormattedMessage id={"december"} key={11}/>,
];

export const weakDays = [
    <FormattedMessage id={"monday"} key={0}/>,
    <FormattedMessage id={"tuesday"} key={1}/>,
    <FormattedMessage id={"wednesday"} key={2}/>,
    <FormattedMessage id={"thursday"} key={3}/>,
    <FormattedMessage id={"friday"} key={4}/>,
    <FormattedMessage id={"saturday"} key={5}/>,
    <FormattedMessage id={"sunday"} key={6}/>,
];

export const weekDaysShort = [
    <FormattedMessage id={"mon"} key={0}/>,
    <FormattedMessage id={"tue"} key={1}/>,
    <FormattedMessage id={"wed"} key={2}/>,
    <FormattedMessage id={"thu"} key={3}/>,
    <FormattedMessage id={"fri"} key={4}/>,
    <FormattedMessage id={"sat"} key={5}/>,
    <FormattedMessage id={"sun"} key={6}/>,
];

export const languages = {
    [Languages.SK]: <FormattedMessage id={"slovakia"}/>,
    [Languages.CZ]: <FormattedMessage id={"czechia"}/>,
};
