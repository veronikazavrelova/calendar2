// FUNKCIA PRE ZMENU PORADIA INDEXOV DNI

export const convertDayIndex = (dayIndex) => {
    if (dayIndex === 0) {
        dayIndex = 6;
    } else {
        dayIndex--;
    }
    return dayIndex;
};

// FUNKCIA PRE VYPISANIE PRVEHO DNA V TYZDNI
/**
 * @param month mesiace 0-11
 * @param year
 * @returns {Date}
 */
export const getFirstDay = (month, year) => {
    const day = new Date(year, month);
    const dayNumber = convertDayIndex(day.getDay());
    day.setDate(day.getDate() - dayNumber);
    return day;
};

// FUNKCIE PRE ODPOCITAVIE A PRIPOCITAVANIE ROKOV
// odpocitavanie rokov
export const minusDrawYear = (drawMonth, drawYear) => {
    drawMonth--;
    if (drawMonth === -1) {
        drawMonth = 11;
        drawYear--;
    }
    return [drawMonth, drawYear];
};

// pridavanie rokov
export const plusDrawYear = (drawMonth, drawYear) => {
    drawMonth++;
    if (drawMonth === 12) {
        drawMonth = 0;
        drawYear++;
    }
    return [drawMonth, drawYear];
};

// FUNKCIA PRE VYPOCET CISLA TYZDNA
export const getWeekNumber = (d) => {
    // Copy date so don't modify original
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
    // Get first day of year
    let yearStart;
    yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
    // Calculate full weeks to nearest Thursday
    const weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
    // Return array of year and week number
    return weekNo;
};

// FUNKCIA PRE VYKRESLOVANIE MIEN A SCVIATKOV
export const drawIssues = (issue, language, firstDay) => {
    // podmienka pre zistenie prazdnych mien a sviatkov
    if (!issue[language][firstDay.getMonth()] || !issue[language][firstDay.getMonth()][firstDay.getDate()]) {
        return "";
    }
    return issue[language][firstDay.getMonth()][firstDay.getDate()];
};

 // FUNKCIA PRE VYPOCET ROZDIELU DNI
export const dateDiffInDays = (dayFirst, daySecond) => {
    const _MS_PER_DAY = 1000 * 60 * 60 * 24;
    const utc1 = Date.UTC(dayFirst.getFullYear(), dayFirst.getMonth(), dayFirst.getDate());
    const utc2 = Date.UTC(daySecond.getFullYear(), daySecond.getMonth(), daySecond.getDate());
    return Math.floor((utc2 - utc1) / _MS_PER_DAY);
};
