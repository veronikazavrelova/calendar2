import czechHolidaysFixed from "../locales/czechHolidays";
import slovakHolidaysFixed from "../locales/slovakHolidays";
import {Languages} from "./Languages";

// FUNKCIA PRE VYPOCET VELKEJ NOCI A ZJEDNOTENIE SVIATKOV
export const initHolidays = (actualDrawDate) => {
    // deepcopy objektu slovakholidaysfixed
    const slovakHolidays = {};
    for (const key in slovakHolidaysFixed) {
        slovakHolidays[key] = {};
        for (const key2 in slovakHolidaysFixed[key]) {
            slovakHolidays[key][key2] = slovakHolidaysFixed[key][key2];
        }
    }
    // deepcopy objektu czechholidaysfixed
    const czechHolidays = {};
    for (const key in czechHolidaysFixed) {
        czechHolidays[key] = {};
        for (const key2 in czechHolidaysFixed[key]) {
            czechHolidays[key][key2] = czechHolidaysFixed[key][key2];
        }
    }

    const holidays = {
        [Languages.SK]: slovakHolidays,
        [Languages.CZ]: czechHolidays,
    };

    // vypocet dna velkonocnej nedele
    let eastDay;
    let eastMonth;
    const eastYear = actualDrawDate.getFullYear();
    const eastDate = new Date(actualDrawDate.getTime());
    const a = eastYear % 19;
    const b = eastYear % 4;
    const c = eastYear % 7;
    const m = 24;
    const n = 5;

    const d = ((19 * a) + m) % 30;
    const e = (n + (2 * b) + (4 * c) + (6 * d)) % 7;

    eastDay = 22 + d + e;
    eastMonth = 2;

    if (eastDay > 31) {
        let p = d + e - 9;
        if (p > 25) {
            p = p - 7;
        }
        eastDay = p;
        eastMonth = 3;
    }

    // funkcia pre vypisanie velkonocnych dni
    const setEastHoliday = (countryHolidays, holidayName) => {
        if (!countryHolidays[eastDate.getMonth()]) {
            countryHolidays[eastDate.getMonth()] = {};
        }
        countryHolidays[eastDate.getMonth()][eastDate.getDate()] = holidayName;
    };
    // velokoncna nedela
    eastDate.setMonth(eastMonth, eastDay);
    setEastHoliday(slovakHolidays, "Veľkonočná nedeľa");
    setEastHoliday(czechHolidays, "Velikonoční neděle");
    // velkonocny pondelok
    eastDate.setMonth(eastMonth, eastDay + 1);
    setEastHoliday(slovakHolidays, "Veľkonočný pondelok");
    setEastHoliday(czechHolidays, "Velikonoční pondělí");
    // velkonocny piatok
    eastDate.setMonth(eastMonth, eastDay - 2);
    setEastHoliday(slovakHolidays, "Veľký piatok");
    setEastHoliday(czechHolidays, "Velký pátek");
    return holidays;
};
