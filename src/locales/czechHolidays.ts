export default {
    0: {
        1: "Den obnovy samostatného českého státu",
    },
    4: {
        1: "Svátek práce",
        8: "Den vítězství",
    },
    6: {
        5: "Den slovanských věrozvěstů Cyrila a Metoděje",
        6: "Den upálení mistra Jana Husa",
    },
    8: {
        28: "Den české státnosti",
    },
    9: {
        28: "Den vzniku samostatného československého státu",
    },
    10: {
        17: "Den boje za svobodu a demokracii",
    },
    11: {
        24: "Štědrý den",
        25: "1. svátek vánoční",
        26: "2. svátek vánoční",
    },
};
