
export default {
    0  : {
        1  : "Deň vzniku Slovenskej republiky",
        6  : "Zjavenie Pána (Traja králi)",
    },
    4  : {
        1  : "Sviatok práce",
        8  : "Deň víťazstva nad fašizmom",
    },
    6  : {
        5  : "Sviatok svätého Cyrila a Metoda",
    },
    7  : {
        29  : "Výročie SNP",
    },
    8  : {
        1  : "Deň Ústavy Slovenskej republiky",
        15  : "Sedembolestná Panna Mária",
    },
    10  : {
        1  : "Sviatok všetkých svätých",
        17  : "Deň boja za slobodu a demokraciu",
    },
    11  : {
        24  : "Štedrý deň",
        25  : "Prvý sviatok vianočný",
        26  : "Druhý sviatok vianočný",
    },
};
